package com.accenture.githubrepositories.pojo

data class PullRequestUser (
     var login : String = "",
     var id : String = "",
     var avatar_url : String = ""
)