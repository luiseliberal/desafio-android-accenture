package com.accenture.githubrepositories.pojo

data class RepositoryUser (
     var id : Int = 0,
     var login : String = "",
     var avatar_url : String = ""
)